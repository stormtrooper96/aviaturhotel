const JavaScriptObfuscator = require('javascript-obfuscator');
const fs = require('fs');
const obfuscate = () => {
   fs.readFile('server.js','utf8', function(err, data) {
      if (err) {console.log(err)};
      let obfuscationResult = JavaScriptObfuscator.obfuscate(data);
      let uglyCode = obfuscationResult.getObfuscatedCode();
      fs.writeFile('dist/server.min.js', uglyCode, function (err) {
         if (err) throw err;

      });
    })
};
obfuscate();