var JavaScriptObfuscator = require('javascript-obfuscator');
var fs = require('fs');
var codigo =fs.openSync('server.js')
console.log(codigo)
var obfuscationResult = JavaScriptObfuscator.obfuscate(
codigo,
    {
        compact: false,
        controlFlowFlattening: true,
        controlFlowFlatteningThreshold: 1,
        numbersToExpressions: true,
        simplify: true,
        shuffleStringArray: true,
        splitStrings: true,
        stringArrayThreshold: 1
    }
);







fs.writeFileSync('server.min.js', obfuscationResult.getObfuscatedCode());