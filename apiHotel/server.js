const express = require('express');
const app = express();
const cors = require('cors');

const {readFile, writeFile} = require('fs').promises;
const { stat } = require('fs');

app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(express.json());

let hotels = [];

app.get('/', (req, res) => {
    res.send('Servidor de prueba');
});





app.get('/hotels', async (req, res) => {
    await open();
    res.json(hotels);
});
app.get('/hotels/:id', async(req, res) => {
    await open();
    const id=req.params.id;

    const findExist = hotels.find(hotel => hotel.id === id);
    if (!findExist) {
        return res.status(404).send({error: true, msg: 'Hotel No encontrado'})
    }

    hotels = hotels.filter(hotel => hotel.id === id);
    res.json(hotels);

});
app.patch('/hotels/:id', async (req, res) => {
    await open()
    const id = req.params.id;
  
    const findExist = hotels.find(hotel => hotel.id === id);
    if (!findExist) {
        return res.status(404).send({error: true, msg: 'Hotel No encontrado'})
    }
    hotel = hotels.filter(hotel => hotel.id === id);
 ;
    hotel.push(req.body);
    await save();
    res.json(hotels);
});
app.delete('/hotels/:id', async (req, res) => {
    const id = req.params.id;
    await open();
    hotels = hotels.filter(hotel => hotel.id !== id);
    await save();
    res.json(hotels);
});

app.post('/hotels', async (req, res) => {

    
    const hotelData = req.body
    if (hotelData.id == null || hotelData.name == null || hotelData.price == null || hotelData.stars == null) {
        return res.status(401).send({error: true, msg: 'Se requiere mayor información pra el hotel'})
    }
    await open();
    hotels.unshift(req.body);
    
    await save();
    res.json(req.body);
});

async function save(){
    const res = await writeFile('data.json', JSON.stringify(hotels), 'utf-8');
}

async function open(){
    const res = await readFile('data.json', 'utf-8');
    hotels = JSON.parse(res);

}





app.listen(3000, () => {
    console.log('servidor iniciado...');
});