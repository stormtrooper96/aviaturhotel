# Api Hoteles Aviatur




En esta api Rest se encuentran los metodos para crear,eliminar y verlos detalles de los hoteles disponibles del banco de datos en un archivo JSON 


## Features

- Crear Hotel  
- Eliminar Hotel
- ver detalle de hotel
- Actulizar hotel(en desarrollo)
- Lectura y escritura de Archivos Json




## Tecnologias usadas

Para este Proyecto se usan las siguientes tecnologias:


- [node.js] - es un entorno que trabaja en tiempo de ejecución, de código abierto, multi-plataforma
- [Express] - Es un framework de node que maneja bastantes funcionalidades dentro de las cules permite manejar las peticiones  




## Instalación

Api Hoteles Aviatur requires [Node.js](https://nodejs.org/) v14+ Para correr

Instalando las dependencias y poniendolo en marcha
para correr este comando es necesario hacerlo desde la carpeta del proyecto en angular
Para el entorno de desarrollo correr este comando
```sh
cd apiHotel
npm i
npm run start
```

Para el ambiente de producción 

```sh
cd apiHotel
npm i
npm run build
npm run prod-server
```
Por defecto la url es 
```sh
localhost:3000
```

## Estructura Archivo
 Para Utilizar la información se tiene el siguiente modelo

id= numero de id del hotel es de tipo numerico
name: Nombre del hotel, este campo es de tipo texto
stars: Numero de estrellas que tiene el hotel es de tipo numerico
price:	Numero de estrellas que tiene el hotel es de tipo numerico
image:	Nombre de la imagen el cual debe corresponder al de la carpeta assest, este campo es de tipo texto
amenities[] es un arreglo de texto con todas las caracteristicas que tiene el hotel

 Por ejemplo
 ```json
id:	"161901"
name:	"Hotel Santa Cruz"
stars:	3
price:	1267.57
image:	"6623490_6_b.jpg"
amenities[	
	"nightclub"
	"business-center"
	"bathtub"
	"newspaper"
	"restaurant"
]
```
## Rutas
Para el api se tienen las siguientes rutas

| Ruta  |  Descripción  | Tipo   |  Parametros|
|---|---|---|---|
|  http://localhost:3000/hotels |  Devuelve la lista de hoteles  | Get   | N/A   |
|    http://localhost:3000/hotels/:id| Devuelve el hotel que se encuentra con el identificador :id   |Get   | id   |
|    http://localhost:3000/hotels/:id| Remueve el hotel que se encuentra con el identificador :id   |Delete   | id   |
|    http://localhost:3000/hotels| Crea El hotel con el contenido body, este debe encontrarse en formato json   |Post   | id   |
|    http://localhost:3000/hotels/:id| Actualiza el hotel que se encuentra con el identificador :id   |Pacht   | id   |


