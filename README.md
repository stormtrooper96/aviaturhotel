# AviaturHotel

Este proyecto fue generadocon [Angular CLI](https://github.com/angular/angular-cli) version 11.2.3.





En esta App se encuentra la logica de muestra y renderizado de componenentes para hoteles optimizado para ser responsive


## Features

- Vista Responsiva
- ApiRest en Express  [ver README](https://gitlab.com/stormtrooper96/aviaturhotel/-/tree/master/apiHotel)
- Filtrar Por Nombre y estrellas
- Envio y procesamiento de solicitudes http




## Tech

Para este Proyecto se usan las siguientes tecnologias:


- [node.js] - Es un entorno que trabaja en tiempo de ejecución, de código abierto, multi-plataforma
- [Angular] - Es un framework opensource desarrollado por Google para facilitar la creación y programación de aplicaciones web de una sola página, las webs SPA (Single Page Application). 
- [Bootstrap] - Es una libreria que  facilita el manejo de de elementos HTMl y Css, facilitando tareas de responsive




## Instalación
Instalando las dependencias y poniendolo en marcha para correr esta aplicacióón es necesario hacerlo desde la carpeta principal del proyecto:
AviaturHotel requiere 
[Node.js](https://nodejs.org/) v14+ Para correr

Para instalar las dependencias se debe ejecutar el comando

`npm i` 


## Servidor de desarrollo
Ejecute `ng serve` para un servidor de desarrollo. Navega hasta `http://localhost:4200/`. La aplicación se recargará automáticamente si cambias alguno de los archivos fuente.



## Construir 

Ejecute `ng build` para construir el proyecto. Los artefactos de construcción se almacenarán en el directorio `dist/`. Utilice el tag `--prod` para una construcción de producción, este comando creara los archivos ofuscados y minificados necesarios para su despliegue.




