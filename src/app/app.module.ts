import { NgModule } from '@angular/core';
import {  ReactiveFormsModule,FormsModule} from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
 import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HotelComponent } from './components/hotel/hotel.component';
import { FilterBarComponent } from './components/filter-bar/filter-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FilterBarModule} from './components/filter-bar/filter-bar.module';
import { HotelsComponent } from './components/hotels/hotels.component';
import { SearchertextPipe } from './pipes/searchertext.pipe'
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HotelComponent,
    FilterBarComponent,
    
    HotelsComponent,
    
    SearchertextPipe,
    

  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FilterBarModule,
    HttpClientModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
