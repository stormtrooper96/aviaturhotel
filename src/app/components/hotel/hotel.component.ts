import { Component, Input, OnInit } from '@angular/core';
import { Hotel } from 'src/app/models/Hotel';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
@Input() hotel:Hotel=new Hotel();

numStarts(n: number): Array<number> { 
  return Array(n); 
}
  constructor() { }

  ngOnInit(): void {
    
  }

}
