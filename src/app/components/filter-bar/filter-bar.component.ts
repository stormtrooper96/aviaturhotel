import { templateJitUrl } from '@angular/compiler';
import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.css'
  
]
})
export class FilterBarComponent implements OnInit {

  @Output('searchInput') searchEmitter= new EventEmitter<string>();
  @Output ('searchStars') searchStarEmitter = new EventEmitter <number>(); 


  showSideBar:boolean=true;

  constructor() {
    if (this.isMobile()) this.showSideBar=false
    else this.showSideBar=true

   }

   isMobile(){
    if (window.matchMedia("(min-width: 700px)").matches) {
      return false;
    } else {
      return true;
    }

   }


   toogleMenu(){this.showSideBar=!this.showSideBar}

filterbystars(star:number){
this.searchStarEmitter.emit(star)


}



   numStarts(n: number): Array<number> { 
    return Array(n); 
  }
   ngOnInit(): void {
   this.searchInput.valueChanges.pipe(debounceTime(300)).subscribe(value=>this.searchEmitter.emit(value))

  }
  searchInput=new FormControl('');
  searchStars=new FormControl('');
  
}
