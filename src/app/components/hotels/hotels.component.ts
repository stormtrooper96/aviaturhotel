import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Hotel } from '../../models/Hotel'
import { HotelService } from "../../services/hotel.service";
@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  ischeckedstar :boolean=false;
  hotels: Hotel[] = [];
  hotelstemporal:Hotel[]=[];
  handleSearch(value: string) {
    this.filterTextValue = value;
  }
  handleStar(star:number){
    let arrayhotelsfilter=[]


    this.ischeckedstar=!this.ischeckedstar
    if (this.ischeckedstar && star !=0){
   
      arrayhotelsfilter= this.hotels.filter(hotel=>hotel.stars==star)
      this.hotels=arrayhotelsfilter  
    }

    else if(star==0){this.hotels=this.hotelstemporal}
    else {this.hotels=this.hotelstemporal}

  }
  GetAmenitesbyhotel(id: number) {
    for (var _i = 0; _i < this.hotels.length; _i++) {
      if (this.hotels[_i].id == id) {
        return new Array(this.hotels[_i].amenities)
      }

    }
    return null
  }

    showallHotels(){
      this.hotels=this.hotelstemporal

    }
  filterTextValue = ''
  filterStarValues=[]


  constructor(private hotelService: HotelService) { }

  ngOnInit(): void {
    this.hotelService.getHotels().subscribe(hotels => {
      this.hotels = hotels;
      this. hotelstemporal=hotels

    });
  }
}
