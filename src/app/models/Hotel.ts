
export class Hotel{
    id:number=0;
    name: string="";
    price: number=0.5;
    image: string="";
    stars:number=1;
    amenities: string[] = [];
}