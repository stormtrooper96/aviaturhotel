import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';
import { Hotel } from '../models/Hotel';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  private url = 'http://localhost:3000/hotels/';
  private httpOptions = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  constructor(private http:HttpClient) { }

    getHotels():Observable<Hotel[]>{
      return this.http.get<Hotel[]>(this.url);
    
  }

}
