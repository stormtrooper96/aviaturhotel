import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'searchertext'
})
export class SearchertextPipe implements PipeTransform {

  transform(values: any[] , filter:string): any[] {
    if (!values) {
      return [];
    }
    if (!filter) {
      return values;
    }
    return values.filter((hotel: any) => hotel.name.toUpperCase().includes(filter.toLocaleUpperCase()));
  
  }

}
