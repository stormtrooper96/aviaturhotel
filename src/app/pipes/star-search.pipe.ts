import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'starSearch'
})
export class StarSearchPipe implements PipeTransform {


  transform(values: any[] , filter:string): any[] {
    if (!values) {
      return [];
    }
    if (!filter) {
      return values;
    }
    return values.filter((hotel: any) => hotel.stars.includes(filter));

  }
}
